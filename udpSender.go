package main

import (
	"fmt"

	"github.com/0xAX/notificator"
)

var notify *notificator.Notificator

func main() {

	notify = notificator.New(notificator.Options{
		DefaultIcon: "Icon_Mediaserver.ico",
		AppName:     "My test App",
	})

	err := notify.Push("title", "text", "Icon_Mediaserver.ico", notificator.UR_CRITICAL)

	if err != nil {
		fmt.Println(err)
	}
}
