package csmanager

import (
	"bytes"
	"fmt"
	"os/exec"

	Models "../csModel"
)

// StartPlayback starts playback of given show in VLC.
func StartPlayback(showItem *Models.ShowItem) {

	// First exit all running instances of VLC.
	killAllPlayerInstances()

	// Now run VLC with show as parameter.
	pathVlc := "C:\\Program Files\\VideoLAN\\VLC\\vlc.exe"
	//pathVlc := filepath.FromSlash(CurrentConfig.FullPathVlc)

	cmd := exec.Command(pathVlc, showItem.FullPath, "--fullscreen")

	var out bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	fmt.Println("Starting playback of show: " + showItem.Title)

	if err != nil {
		fmt.Println("Error: " + stderr.String())
		return
	}

}
