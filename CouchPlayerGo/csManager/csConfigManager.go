package csmanager

import (
	"encoding/json"
	"fmt"
	"os"

	Models "../csModel"
)

// Public functions.
func GetCurrentConfig() Models.Config {

	return processConfigFile()
}

func GetVersion() string {

	return processConfigFile().Version
}

// Private functions.
func processConfigFile() Models.Config {

	var currentConfig Models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	return currentConfig
}
