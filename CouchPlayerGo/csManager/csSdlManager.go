package csmanager

import (
	"fmt"

	Models "../csModel"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

var err error
var Window *sdl.Window
var Renderer *sdl.Renderer
var FontShowItem *ttf.Font
var FontHeader *ttf.Font

// InitSdlSubsysten initialized SDL.
func InitSdlSubsysten(config *Models.ConfigSdl) error {

	// Init SDL.
	err := sdl.Init(sdl.INIT_EVERYTHING)

	if err != nil {
		fmt.Println("Error initializing SDL: ", err)
	}

	setWindowDimensions(config)
	Window = createWindow(config)
	Renderer = createRenderer(Window)
	initFonts()

	return err

}

func setWindowDimensions(config *Models.ConfigSdl) {

	// Get current screen resolution.
	mode, err := sdl.GetCurrentDisplayMode(0)

	if err != nil {
		fmt.Println("Error getting current display mode: ", err)
		return
	}

	config.ScreenHeight = mode.H - 100
	config.ScreenWidth = mode.W - 100
}

func createWindow(config *Models.ConfigSdl) *sdl.Window {

	sdlflags := uint32(sdl.WINDOW_OPENGL)
	sdlflags |= sdl.WINDOW_BORDERLESS

	// Create main window.
	Window, err = sdl.CreateWindow(
		"CouchPlayer", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, config.ScreenWidth, config.ScreenHeight, sdlflags)

	if err != nil {
		fmt.Println("Error creating SDL window: ", err)
	}

	// Set window opacity.
	err = Window.SetWindowOpacity(config.WindowOpacity)

	if err != nil {
		fmt.Println("Error setting opacity: ", err)
	}

	// Set SDL windows icon.
	img.Init(img.INIT_JPG)
	surfaceImg, _ := img.Load("images/Icon_Mediaserver.png")
	Window.SetIcon(surfaceImg)

	return Window
}

func createRenderer(window *sdl.Window) *sdl.Renderer {

	// Create renderer.
	Renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)

	if err != nil {
		fmt.Println("Error creating renderer: ", err)
	}

	return Renderer

}

func initFonts() {

	// Init and open fonts.
	if err := ttf.Init(); err != nil {
		fmt.Printf("Failed to initialize TTF: %s\n", err)
	}

	if FontShowItem, err = ttf.OpenFont("fonts/calibril.ttf", 20); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

	if FontHeader, err = ttf.OpenFont("fonts/calibril.ttf", 40); err != nil {
		fmt.Printf("Failed to open font: %s\n", err)
	}

}
