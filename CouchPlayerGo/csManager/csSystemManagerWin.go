package csmanager

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"

	Models "../csModel"
)

func killAllPlayerInstances() {

	fmt.Println("Terminating player instances.")

	cmd := exec.Command("taskkill", "/IM", "vlc.exe")

	var out bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		// "Der Prozess "vlc.exe" wurde nicht gefunden"
		fmt.Println("Error: " + stderr.String())
		return
	}

}

func DeleteShowFile(showItem *Models.ShowItem) {

	var err = os.Remove(showItem.FullPath)

	fmt.Println("Deleting show: " + showItem.FullPath)

	if err != nil {
		fmt.Println("Error: " + err.Error())
		return
	}

}
