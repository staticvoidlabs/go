package csmanager

import (
	"encoding/json"
	"io/ioutil"
	"os"

	Models "../csModel"
)

func InitShowsRepo() Models.ShowsRepo {

	// Initialize the shows array.
	var shows Models.ShowsRepo

	jsonFile, _ := os.Open("test.json")
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// Unmarshal the byte array which contains the json content into 'shows'
	json.Unmarshal(byteValue, &shows)

	return shows
}
