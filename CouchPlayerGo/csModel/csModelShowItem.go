package csmodel

import (
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

// ShowItem represents a show item.
type ShowItem struct {
	ShowID          int
	FullName        string
	Title           string
	TitleShort      string
	TitlDetail      string
	Origin          string
	OriginShort     string
	OriginLogoScale float32
	Duration        int
	AgeInDays       int
	FullPath        string
	Thumbnail       string
	DateAdded       time.Time
	DateBroadcast   string
	Format          string
}

type SimpleSdlShowItem struct {
	IsSelected          bool
	IsContextMenuActive bool
	Surface             *sdl.Surface
	SurfaceSelected     *sdl.Surface
	SurfaceContext      *sdl.Surface
	Texture             *sdl.Texture
	TextureSelected     *sdl.Texture
	TextureContext      *sdl.Texture
}

// SetAgeInDays sets the age of the given show item based on the field DateAdded.
func (s *ShowItem) SetAgeInDays() {
	s.AgeInDays = int(time.Now().Sub(s.DateAdded).Hours()/24) + 1
}
