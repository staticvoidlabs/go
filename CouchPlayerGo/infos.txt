
// Generate exe without console window
go build -ldflags -H=windowsgui


// Icon
Icon should be 64x64 pixel PNG file
https://stackoverflow.com/questions/46614586/sdl2-no-window-icon-using-sdl-setwindowicon
https://www.flaticon.com/de/suche?word=play&license=selection&order_by=4&grid=small

